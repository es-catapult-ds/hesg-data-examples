# Background
The elimination of carbon emissions from home heating is likely to be required by 2050 in the UK, with similar reductions needed worldwide. To help achieve this a Living Lab data set has been released to allow inventors, academics, and energy service providers to develop their ideas and concepts. The data set comes from real homes, capturing how participants use their heating. This includes the temperatures of all rooms, the temperature they requested and whether the requests were made in advance or for immediate effect. In addition, it provides the gas and electricity use of the homes and details of the local weather conditions. No directly comparable dataset exists. The currently available data covers a period from 2017 to 2020. The Living Lab continues to provide a facility where innovators and academics can try out new ideas and will continue to make further data available on home heating and other home energy use, such as electric vehicle use.

# hesg-data-examples
These examples are demonstrating what can be done with the Energy Systems Catapult (ESC) Living Lab data, which was collected using the Home Energy Services Gateway (HESG). The notebook examples use the data from October 2018 to September 2019, but will work on any of the data sets listed below.

### More information on HESG and the Living Lab:
[Living Lab 1 Public Data Description](Living Lab 1 Public Data Description v1.0.pdf)  
[ESC Living Lab](https://es.catapult.org.uk/capabilities/digital-and-data/living-lab/)

### The ESC HESG Data from October 2017 to September 2018 is available [here](http://doi.org/10.5255/UKDA-SN-8602-1).

### The ESC HESG Data from October 2018 to September 2019 is available here:

Sensor Data:  
[eden1](https://usmart.io/org/esc/discovery/discovery-view-detail/2e887717-3c33-4d46-9167-95be307289a2)
[eden2](https://usmart.io/org/esc/discovery/discovery-view-detail/214f35bf-68e9-4762-bbae-f0094a1c469e)
[eden3](https://usmart.io/org/esc/discovery/discovery-view-detail/e8cacfb0-8e95-4d5d-855f-b164be8e2f41)
[eden4](https://usmart.io/org/esc/discovery/discovery-view-detail/337291db-50ea-4d18-b2db-2288e8aca921)
[eden5](https://usmart.io/org/esc/discovery/discovery-view-detail/a004a8f1-af11-4628-ba0a-12e2eae76790)

Weather Data:  
[eden1](https://usmart.io/org/esc/discovery/discovery-view-detail/92b5a675-3ba2-472d-ad3d-62a048a4132a)
[eden2](https://usmart.io/org/esc/discovery/discovery-view-detail/bbaaacba-8378-45ba-b178-fe56b2b5b27d)
[eden3](https://usmart.io/org/esc/discovery/discovery-view-detail/b76a1a5d-e9fc-4a8f-b52f-750b6c403746)
[eden4](https://usmart.io/org/esc/discovery/discovery-view-detail/332517c4-2a84-4eb8-b7a6-7408fa1e2b39)
[eden5](https://usmart.io/org/esc/discovery/discovery-view-detail/bdfe403f-deac-4a2c-8ac0-81a7944d6fb7)

User Heating Schedule:  
[eden1](https://usmart.io/org/esc/discovery/discovery-view-detail/bc13282c-894f-470b-86a7-b780c4b39f8c)
[eden2](https://usmart.io/org/esc/discovery/discovery-view-detail/8903c602-1b91-497f-adcc-0fcf07ecd82d)
[eden3](https://usmart.io/org/esc/discovery/discovery-view-detail/5ab405b9-3be3-4900-a33d-fa200f7a9d0b)
[eden4](https://usmart.io/org/esc/discovery/discovery-view-detail/4296289d-f9ba-422d-a72a-7fd57c6f4baf)
[eden5](https://usmart.io/org/esc/discovery/discovery-view-detail/848a1d8d-756e-4e34-8eea-6f094113c06c)

User Overrides:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/b285f84a-7b62-478c-ab6b-fded7cc0a4a5)

Home Model:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/dac22c94-4089-4af3-82a5-36c8867e71ec)

Home Details:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/651701e5-7322-4bcf-83fe-633e6eb3ae7d)

Heating Targets:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/1b3ae07a-4bc0-4279-86d2-63c3977dc7a6)

### The ESC HESG Data from October 2019 to May 2020 is available here:

Sensor Data:  
[eden1](https://usmart.io/org/esc/discovery/discovery-view-detail/79559d9c-9460-4ca4-a1a2-de668edfa353)
[eden2](https://usmart.io/org/esc/discovery/discovery-view-detail/e60dce68-c326-428b-8e7f-37c32cdff9a2)
[eden3](https://usmart.io/org/esc/discovery/discovery-view-detail/5bdcf394-7c72-461d-a3a5-b443581cbaf2)
[eden4](https://usmart.io/org/esc/discovery/discovery-view-detail/9df293c5-6307-4023-8fbb-423cd21daa06)
[eden5](https://usmart.io/org/esc/discovery/discovery-view-detail/4ad5ffa0-dea1-4bb2-a464-a78e438b5f4f)

Weather Data:  
[eden1](https://usmart.io/org/esc/discovery/discovery-view-detail/94e30c75-e81d-4bd2-88a5-d183ef59f577)
[eden2](https://usmart.io/org/esc/discovery/discovery-view-detail/ab0ae1ca-537c-474f-8205-bb627c5978c0)
[eden3](https://usmart.io/org/esc/discovery/discovery-view-detail/ab0ae1ca-537c-474f-8205-bb627c5978c0)
[eden4](https://usmart.io/org/esc/discovery/discovery-view-detail/d08465ce-1527-4391-a970-d1d37d6963c5)
[eden5](https://usmart.io/org/esc/discovery/discovery-view-detail/427de437-d77e-4817-ab0f-0500b15ea7c9)

User Heating Schedule:  
[eden1](https://usmart.io/org/esc/discovery/discovery-view-detail/d5520346-d867-49e8-92eb-b9d88a34a564)
[eden2](https://usmart.io/org/esc/discovery/discovery-view-detail/38e075e3-7030-4080-a01f-2503fb70be31)
[eden3](https://usmart.io/org/esc/discovery/discovery-view-detail/cfdf1ad2-a143-4395-90f1-8d367f76b862)
[eden4](https://usmart.io/org/esc/discovery/discovery-view-detail/191aef3d-43f9-4a44-8540-4a625f8f9f76)
[eden5](https://usmart.io/org/esc/discovery/discovery-view-detail/5690f836-912c-40db-8452-a854924c5183)

User Overrides:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/914f3670-8b49-4de7-85cc-957257be7b07)

Home Model:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/ab0ae1ca-537c-474f-8205-bb627c5978c0)

Home Details:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/d7601d84-8094-44bb-9dc2-4f5b1cc052c4)

Heating Targets:  
[eden1-5](https://usmart.io/org/esc/discovery/discovery-view-detail/6eda2162-ea7b-45f8-b31d-0d08502cd874)
